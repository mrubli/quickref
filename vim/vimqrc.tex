% VIM Quick Reference Card
%
% Build:
%   apt install texlive-xetex texlive-fonts-extra
%   xelatex -halt-on-error vimqrc.tex
% On Ubuntu 14.04 the following packages may be necessary as well:
%   texlive-latex-recommended texlive-fonts-recommended texlive-latex-extra
%
% This is a hint for vscode:
% !TEX program = xelatex

\documentclass[11pt]{article}

\usepackage[margin=1.2cm,a3paper]{geometry}
\usepackage{multicol}
\usepackage{libertineotf}
\setmonofont[Scale=MatchLowercase]{Latin Modern Mono}
\usepackage{hyperref}
\hypersetup{
    pdfauthor={Martin Rubli},
    hidelinks,
}
\usepackage{mdframed}
\usepackage{xcolor}

% Character definitions for tokens widely used in TeX
\def\bs{$\textbackslash$}
\def\lbrace{\textbraceleft}
\def\rbrace{\textbraceright}
\def\lbracket{[}
\def\rbracket{]}

% Character definitions for special characters
\def\~{\textasciitilde}
\def\^{\textasciicircum}	% Required for libertine, otherwise "\^{}" and "\^\ " render boxes

% Definitions useful for reference cards
\def\ctrl{\textrm{\char94}\kern-1pt}
\def\enter{$\hookleftarrow$}
\def\backspace{$\leftarrow$}
\def\OR{\thinspace{\textit{\tiny or}}\thinspace}	% https://tex.stackexchange.com/questions/28274
\def\key#1{\textit{\textrm{\small{$\langle$#1$\rangle$}}}}
\def\leader{\key{Leader}}
\def\cmd#1{\texttt{#1}}

\def\eol{\hfil\break}
\def\title#1{\hfil{\textbf{#1}}\hfil\par\vskip 2pt\hrule}
\def\cm#1#2{{\texttt{#1}}\dotfill#2\par}	% Two columns
\def\cn#1{\hfill$\lfloor$ #1\par}			% Continuation
\def\section#1{\medskip \textbf{#1}\par}
\def\highlight#1{\textcolor{red}{#1}}

%\linespread{0.95}
\setlength{\parindent}{0pt}
\pagenumbering{gobble}

% Use a sans-serif font
\renewcommand{\familydefault}{\sfdefault}
\urlstyle{same}		% Use the default font for URLs as well

% Style-definitions for mdframed
\newmdenv[
	topline=false,
	bottomline=false,
	rightline=false,
	linewidth=2pt,
	leftmargin=2pt,
	innerleftmargin=6pt,
	innerrightmargin=0pt,
	skipabove=2pt,
	innertopmargin=3pt,
	skipbelow=2pt,
	innerbottommargin=3pt,
]{LeftLine}

\begin{document}

\title{VIM Quick Reference Card}

\begin{multicols}{3}

\cm{:viusage }{Show a summary of all commands }

\section{Movements}
\cm{h l k j }{character left, right; line up, down }
\cm{b w }{word/token left, right }
\cm{ge e }{end of word/token left, right}
\cm{\lbrace\ \rbrace }{beginning of previous, next paragraph}
\cm{( ) }{beginning of previous, next sentence}
\cm{0 \^\ \$ }{beginning, first, last character of line}
\cm{$n$G $n$gg }{line $n$, default the last, first}
\cm{$n$| }{column $n$ of current line}
\cm{\% }{match of next brace, bracket, comment, \texttt{\#define}}
\cm{- + }{line up, down on first non-blank character}
\cm{B W }{space-separated word left, right}
\cm{gE E }{end of space-separated word left, right}
\cm{g0 gm }{beginning, middle of \textit{screen} line}
\cm{g\^\ g\$ }{first, last character of \textit{screen} line}
\cm{f$c$ F$c$ }{next, previous occurence of character $c$}
\cm{t$c$ T$c$ }{before next, previous occurence of $c$}

\section{Insertion \& Replace $\to$ insert mode}
\cm{i a }{insert before, after cursor}
\cm{I A }{insert at beginning, end of line}
\cm{gI }{insert text in first column}
\cm{o O }{open a new line below, above the current line}
\cm{r$c$ }{replace character under cursor with $c$}
\cm{gr$c$ }{like \texttt{r}, but without affecting layout}
\cm{R }{replace characters starting at the cursor}
\cm{gR }{like \texttt{R}, but without affecting layout}
\cm{c$m$ }{change text of movement command $m$}
\cm{cc\OR S }{change current line \highlight{and autoindent}}
\cm{C }{change to the end of line}

\section{Deletion}
\cm{x X }{delete character under, before cursor}
\cm{d$m$ }{delete text of movement command $m$}
\cm{dd D }{delete current line, to the end of line}
\cm{J gJ }{join current line with next, without space}
\cm{:$r$d } {delete range $r$ lines}
\cm{:$r$d$x$ } {delete range $r$ lines into register $x$}

\section{Insert Mode}
\cm{\ctrl V$c$ \ctrl V$nnn$ \ctrl Vx$nn$ }{insert char $c$ literally, by value $n$}
\cm{\ctrl Vu$nnnn$ \ctrl VU$nnnnnnnn$ }{insert Unicode character}
\cm{\ctrl V$n$ }{insert decimal value of character}
\cm{\ctrl A }{insert previously inserted text}
\cm{\ctrl @ }{same as \texttt{\ctrl A} and stop insert $\to$ command mode}
\cm{\ctrl R$x$ \ctrl R\ctrl R$x$ }{insert content of register $x$, literally}
\cm{\ctrl N \ctrl P }{text completion before, after cursor}
\cm{\ctrl W }{delete word before cursor}
\cm{\ctrl U }{delete all inserted character in current line}
\cm{\ctrl D \ctrl T }{shift left, right one shift width}
\cm{\ctrl O$c$ }{execute $c$ in temporary command mode}
\cm{\ctrl X\ctrl E \ctrl X\ctrl Y }{scroll up, down}
\cm{\key{Esc}\OR \ctrl\lbracket}{abandon edition $\to$ command mode}
\cm{\ctrl R= }{\highlight{insert result of arithmetic expression}}

\section{Search \& Substitution}
\cm{/$s$\enter\ ?$s$\enter }{search forward, backward for $s$}
\cm{/$s$/$o$\enter\ ?$s$?$o$\enter }{search fwd, bwd for $s$ with offset $o$}
\cn{  \highlight{offset: $n$: $n$ lines; \texttt{e$n$ s$n$}: $n$ chars from end/start}}
\cm{n\OR /\enter }{repeat forward last search}
\cm{N\OR ?\enter }{repeat backward last search}
\cm{\# * }{search backward, forward for word under cursor}
\cm{g\# g* }{same, but also find partial matches}
\cm{gd gD }{local, global definition of symbol under cursor}
\cm{:$r$s/$f$/$t$/$x$ } {substitute $f$ by $t$ in range $r$}
\cn{$x:$ \texttt{g}---all occurrences, \texttt{c}---confirm changes}
\cm{:$r$s $x$ } {repeat substitution with new $r$ \& $x$}
\cm{:s/\bs\%V$f$\bs\%V/$t$/$x$ } {\highlight{substitute within visual area}}
\cm{gn $o$gn}{\highlight{repeat search and select, run $o$ on result}}

\section{Formatting/Filtering}
$m$ is movement; leave out for visual mode commands \eol
\cm{gq$m$}{format $m$ (\textit{formatprg})}
\cm{gqgq \OR gqip}{format current paragraph}
\cm{$=m$}{reindent movement $m$ (\textit{equalprg})}
\cm{:$r$ce $w$ } {center lines in range $r$ to width $w$}
\cm{:$r$le $i$ } {left align lines in range $r$ with indent $i$}
\cm{:$r$ri $w$ } {right align lines in range $r$ to width $w$}
\cm{!$m$$c$\enter }{filter lines of movement $m$ through command $c$}
\cm{$n$!!$c$\enter }{filter $n$ lines through command $c$}
\cm{:$r$!$c$ } {filter range $r$ lines through command $c$}
\cm{\~ }{switch case and advance cursor}
\cm{g\~{$m$} gu$m$ gU$m$ }{switch case, lc, uc on movement $m$}
\cm{$<$$m$ $>$$m$ }{shift left, right text of movement $m$}
\cm{$n$$<$\kern-3pt$<$ $n$$>$\kern-3pt$>$ }{shift $n$ lines left, right}
\cm{\ctrl A \ctrl X}{increment/decrement number under cursor}

\section{Visual Mode \& Text Objects}
\cm{v V \ctrl V }{highlight characters, lines, block}
\cm{o }{exchange cursor pos with start of highlighting}
\cm{gv }{start highlighting on previous visual area}
\cm{aw as ap }{select a word, a sentence, a paragraph}
\cm{ab aB a] a$>$ at}{select a block ( ), \texttt{\lbrace} \texttt{\rbrace}, [~], $<$~$>$, tag}
\cm{a" a' a`}{select quoted string}
\cn{  \highlight{\texttt{i} instead of \texttt{a} selects ``inner''}}
\cm{$n\hskip -0.3em>$ $n\hskip -0.3em<$ $=$ }{indent/unindent $n$ levels, reindent}

\section{Undoing, Repeating \& Registers}
\cm{u U }{undo last command, restore last changed line}
\cm{.\thinspace\thinspace\ctrl R }{repeat last changes, redo last undo}
\cm{$n$.\ }{repeat last changes with count replaced by $n$ }
\cm{q$c$ q$C$ }{record, append typed characters in register $c$}
\cm{q }{stop recording}
\cm{@$c$ }{execute the content of register $c$}
\cm{@@ }{repeat previous \texttt{@} command}
\cm{:@$c$ } {execute register $c$ as an \textit{Ex} command}

\section{Copying (Yanking)}
\cm{"$x$ }{use register $x$ for next delete, yank, put}
\cm{:reg [$x$] } {show content registers/$x$ reg}
\cm{y$m$ }{yank the text of movement command $m$}
\cm{yy\OR Y }{yank current line into register}
\cm{p P }{put register after, before cursor position}
\cm{\rbracket p \lbracket p }{like \texttt{p}, \texttt{P} with indent adjusted}
\cm{gp gP }{like \texttt{p}, \texttt{P} leaving cursor after new text}

\section{Patterns (differences to Perl) ~~ \texttt{:help pattern}}
\cm{\bs$<$ \bs$>$ }{start, end of word}
\cm{\bs i \bs k \bs I \bs K }{an identifier, keyword; excl. digits}
\cm{\bs f \bs p \bs F \bs P }{a file name, printable char.; excl. digits}
\cm{\bs e \bs t \bs r \bs b }{\key{esc}, \key{tab}, \key{\enter}, \key{$\gets$}}
\cm{\bs = * \bs + }{match $0..1$, $0..\infty$, $1..\infty$ of preceding atoms}
\cm{\bs$\{n,m\}$ }{match $n$ to $m$ occurrences}
\cm{\bs$\{-\}$ }{non-greedy match}
\cm{\bs$|$ }{separate two branches ($\equiv$ \textit{or})}
\cm{\bs( \bs) }{group patterns into an atom}
\cm{\bs \& \bs{}1 }{the whole matched pattern, $1^{st}$ \texttt{()} group}
\cm{\bs u \bs l }{upper, lowercase character}
\cm{\bs c \bs C }{ignore, match case on next pattern}
\cm{\bs \%x }{match hex character}
\cm{\bs @= \bs @! }{\texttt{ (?=$pattern$) (?!$pattern$)}}
\cm{\bs @$<$= \bs @$<$! }{\texttt{ (?$<$=$pattern$) (?$<$!$pattern$)}}
\cm{\bs @$>$ }{\texttt{ (?$>$$pattern$)}}
\cm{\bs\_\^{} \bs\_\$ }{start-of-line/end-of-line}
\cm{\bs\_. }{any single char, including end-of-line}
\cm{\bs zs \bs ze }{set start/end of pattern}
\cm{\bs \%\^{} \bs\%\$ }{match start/end of file}
\cm{\bs\%{}V }{match inside visual area}
\cm{\bs\texttt{'}\textrm{m} }{match with position of mark m}
\cm{\bs\%(\bs) }{unnamed grouping}
\cm{\bs\_$[$ $]$ }{collection with end-of-line included}
\cm{\bs\%$[$ $]$ }{sequence of optionally matched atoms}
\cm{\bs{}v }{very magic: patterns almost like perl}

\section{Marks, Motions, and Tags}
\cm{m$c$ }{mark current position with mark $c\in[a..Z]$}
\cm{`$c$ `$C$ }{go to mark $c$ in current, $C$ in any file}
\cm{`\/`  `\/" }{go to position before jump, at last edit}
\cm{`[ `] }{go to start, end of previously operated text}
\cm{:marks } {print the active marks list}
\cm{:jumps } {print the jump list}
\cm{$n$\ctrl O }{go to $n^{th}$ older position in jump list}
\cm{$n$\ctrl I }{go to $n^{th}$ newer position in jump list}
\cm{\ctrl ] \ctrl T }{jump to the tag under cursor, return from tag}
\cm{:ts $t$ } {list matching tags and select one for jump}
\cm{:tj $t$ } {jump to tag or select one if multiple matches}
\cm{gf }{open file which filename is under cursor}
\cm{:tags } {print tag list}

\section{Multiple Files / Buffers (\enter)}
\cm{:tab ball }{show buffer tablist}
\cm{:buffers }{show list of buffers}
\cm{:buffer n }{switch to buffer n}
\cm{:badd f.txt }{load file into new buffer}
\cm{:bdelete n }{delete buffer n (also with filename)}
\cm{:bnext :bprev :bfirst :blast }{buffer movement}

\section{Scrolling \& Multi-Windowing}
\cm{\ctrl D \ctrl U, \ctrl F \ctrl B}{scroll down, up by half/full page}
\cn{  \highlight{set \texttt{scroll}=$count$ if a count is given}}
\cm{$n$\ctrl E $n$\ctrl Y }{\highlight{scroll down, up $n$ lines}}
\cm{zt zz zb }{current line to top, center, bottom of win}
\cm{:[v]split \ctrl Ws \ctrl Wv} {split window (horiz., vert.)}
\cm{:vert help}{open help vertically}
\cm{\ctrl Wf \ctrl W]}{open file, tag in split}
\cm{:[v]new \ctrl W\ctrl n } {new empty window (horiz., vert.)}
\cm{:on \ctrl Wo} {make current win the only one}
\cm{\ctrl Wj \ctrl Wk \ctrl Wh \ctrl Wl }{move down, up, left, right to win}
\cm{\ctrl WJ \ctrl WK \ctrl WH \ctrl WL }{move win down, up, left, right}
\cm{\ctrl W$n$+ \ctrl W$n$- }{increase/decrease win size by $n$ lines}
\cm{\ctrl W$n>$ \ctrl W$n<$ }{increase/decrease window width}
\cm{\ctrl W$n$\_ \ctrl W$n$|}{set height/width (max if no $n$)}
\cm{\ctrl W=}{Make win size equal}
\cm{\ctrl Wr \ctrl Wx}{Rotate, exchange windows}
\cm{\ctrl Wc}{close window}

\section{Misc Ex Commands (\enter) ~~ \texttt{:help holy-grail}}
\cm{:e $f$ }{edit file $f$, reload current file if no $f$}
\cm{:$r$~w $f$ }{write range $r$ to file $f$ (current file if no $f$)}
\cm{:$r$~w$>$\kern-3pt$>$$f$ }{append range $r$ to file $f$}
\cm{:$r$~g[!]/$p$/$cmd$ }{ex $cmd$ on range $r$ [not] matching $p$}
\cm{:q :q! }{quit and confirm, quit and discard changes}
\cm{:wq\OR :x\OR ZZ }{write to current file and exit}
\cm{:r $f$ }{insert content of file $f$ below cursor}
\cm{:r!\ $c$ }{insert output of command $c$ below cursor}
\cm{:$r$c\ $a$ :$r$m\ $a$ }{copy, move range $r$ below line $a$}
\cm{:\ldots\ctrl D }{list valid inline completions for \ldots}

\section{Ex Ranges}
\cm{, ;\ }{separates two lines numbers, set to first line }
\cm{$n$ }{an absolute line number $n$}
\cm{.\thinspace\thinspace\thinspace\$ }{the current line, the last line in file}
\cm{\% * }{entire file, visual area}
\cm{'$t$ }{position of mark $t$}
\cm{/$p$/ ?$p$? }{the next, previous line where $p$ matches}
\cm{+$n$ -$n$ }{$+n$, $-n$ to the preceding line number}

\section{Compiling}
\cm{:compiler $c$ }{set/show compiler plugins}
\cm{:make } {run \texttt{makeprg}, jump to first error}
\cm{:cope } {navigate errors from make}
\cm{:cn :cp } {display the next, previous error}
\cm{:cl :cf } {list all errors, read errors from file}

\section{Miscellaneous}
\cm{:sh :!$c$ } {start shell, execute command $c$ in shell}
\cm{K }{run \texttt{keywordprg} (man) on word under cursor}
\cm{\ctrl L }{redraw screen}
\cm{\ctrl G }{show cursor column, line, and character position}
\cm{:set cuc } {show cursor column visually}
\cm{ga }{show ASCII value of character under cursor}
\cm{:mkview $[f]$ :loadview $[f]$ }{save/load configuration}
\cm{:set ff=dos } {convert file to dos eol format}
\cm{:e ++ff=unix } {reopen file in unix eol format}
\cm{:write ++enc=utf-8}{Write file in utf-8}
\cm{:set hlsearch } {highlight searches}
%\cm{:$r$hardcopy > file.ps }{print range to ps file}
\cm{:set list }{show listchar characters (tabs etc.)}
\cm{:set mapleader="$key$" }{define \leader{} (default: \bs)}
\cm{:[verbose] map }{list key mappings}

\section{Digraphs}
\cm{:digraphs }{show list of digraphs}
\cm{\ctrl K$c_1$$c_2$}{enter digraph $\{c_1,c_2\}$}
\begin{LeftLine}
\cm{$C$*}			{greek letter $C$ (\texttt{a*}: α, \texttt{D*}: Δ, \ldots)}
\cm{\texttt{DG}}	{° (degree symbol)}
\end{LeftLine}

\section{Modelines}
Format: \texttt{·vim: set ts=$x$ sw=$y$ [no]et} \eol
\cm{:set [no]modeline}	{enable/disable modelines}
\cm{:set modelines=$n$}	{scan $n$ lines from top/bottom}

\newpage

\section{Plugin: dispatch}
\cm{:Start! $cmd$ }		{run command $cmd$ in background}

\section{Plugin: fugitive}
\cm{:Git $cmd$ }		{run Git command $cmd$ }
\cm{:Ggrep }			{run \texttt{git-grep}}
\cm{:Glog }				{load revisions into quickfix list}
\cm{:Gdiff [$rev$]}		{diff against index or revision $rev$}
\cm{:Gblame }			{run \texttt{git-blame} on current file}
\begin{LeftLine}
In the \cmd{:Gblame} window:\par
\cm{g? }				{show help}
\cm{q }					{close blame window}
\cm{<CR> }				{open commit}
\cm{P }					{reblame at $count$-th parent}
\cm{\~ }				{reblame at $count$-th grandparent}
\cm{A C D }				{resize to include author/commit/time col}
\end{LeftLine}
\cm{:Gstatus }			{bring up output of \texttt{git-status}}
\begin{LeftLine}
In the \cmd{:Gstatus} window:\par
\cm{<CR> }				{edit file}
\cm{- }					{add (unstages) or reset (staged)}
\cm{p }					{run \texttt{:Git add}/\texttt{reset --patch}}
\cm{cc ca }				{run \texttt{:Gcommit}(\texttt{ --amend})}
\cm{cvc cva }			{same but with \texttt{--verbose}}
\cm{D }					{run \texttt{:Gdiff}}
\cm{q }					{close}
\cm{U }					{checkout}
\end{LeftLine}
\cm{:Gwrite :Gwq}		{write and stage current file (and quit)}
\cm{:Gcommit }			{run \texttt{git-commit}}
\cm{:Gmove $dest$}		{rename file and buffer}
\cm{:Gdelete}			{delete file and buffer}
\cm{:Gbrowse}			{open in browser at hosting provider}
\cm{y\ctrl G}			{yank hash/path of current object}

\section{Plugin: NERD commenter}
\cm{\leader{}cc }				{comment out current line or selection}
\cm{\leader{}cy }				{same but also yank line/selection}
\cm{\leader{}cu }				{uncomment}
\cm{\leader{}c\key{Spc} ci }	{toggle comment state, per-line}
\cm{\leader{}cm cb}				{comment with multipart, aligned markers}
\cm{\leader{}cs }				{comment with sexy block markers}
\cm{\leader{}c\$ cA }			{comment until, after end of line}
\cm{\leader{}ca }				{switch between delimiter sets}

\section{Plugin: NERDTree}
\cm{:NERDTree }			{open NERDTree file system explorer}
\cm{:NERDTreeFind }		{find current file in tree}
\cm{u U }				{up one directory (leave current open)}
\cm{ma }				{add file below current dir}
\cm{mm md mc}			{move/delete/copy file/dir}
\cm{r}					{refresh cursor dir}
\cm{:NERDTreeClose }	{close NERDTree}
\begin{LeftLine}
In the \cmd{:NERDTree} window:\par
\cm{:Bookmark $name$}		{bookmark current node as $name$}
\cm{:OpenBookmark $name$}	{open bookmark $name$}
\cm{:ClearBookmarks $names$}{clear bookmarks}
\cm{:ClearAllBookmarks}		{clear all bookmarks}
\end{LeftLine}

\section{Plugin: linediff}
\cm{:$r$Linediff }			{set range as left/right block}
\cm{:LinediffReset }		{reset blocks}
\cm{:$r$Linediff\{Add,Last\} }	{add block (and show diff)}
\cm{:LinediffShow }			{show diff}
\cm{:LinediffMerge }		{diff close-by conflict markers}
\cm{:LinediffPick }			{pick buffer to replace conflict area}

\section{Plugin: Command-T}
\cm{\leader{}t b j }		{open file/buffer/jumplist window}

\section{Plugin: Unicode}
\cm{$name$\ctrl{}X\ctrl{}Z }{complete Unicode character}
\cm{:UnicodeName }			{like \texttt{ga} but better}
\cm{:UnicodeSearch $name$ }	{search for Unicode character}
\cm{:Digraphs $name$ }		{search for a digraph}
\cm{:UnicodeTable }			{open Unicode table window}



% Cheap trick to avoid balancing columns on the last page without putting the
% footer on a new page (which is what multicols* would do).
% See: https://tex.stackexchange.com/questions/8683
\vfill\null
\columnbreak
\vfill\null
\columnbreak

\null

\end{multicols}

% Footer
\vfill \hrule \medskip
{\small This card may be freely distributed under the terms of the GNU general public licence ---
Copyright \textcopyright{} 2017 by Martin Rubli.
Based on original by
Michael Goerz  (\url{https://github.com/goerz/Refcards/}) and
Laurent Gr\'egoire  (\url{http://tnerual.eriogerg.free.fr/})}.

\end{document}
